
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}
function pokliciGeneriranje() {
	generirajPodatke(1);
	generirajPodatke(2);
	generirajPodatke(3);	
	alert("Prišlo je do generiranja podatkov, ki so dostopni iz padajočega menuja (pri Preberi EHR zapis...).");
}

/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
	ehrId = "";
	var sessionId = getSessionId();
	console.log("Sejaaa: "+sessionId)
	var names = ["Janez", "Ivan", "Anton"]; //imena izbrana na podlagi najpogostejših slovenskih imen
	var surnames = ["Zdravkotriba","Bolani","Zelobolani"];
	var datesofbirth = ["1958-01-01T00:00:00.000Z","2005-01-01T00:00:00.000Z","1967-01-01T00:00:00.000Z"];
	var arrayofheights = [185,175,180];
	var arrayofweights = [80,50,105];
	var arrayofsbp = [120,130,160];
	var arrayofdbp = [80,85,100];
	var arrayofspo2 = [98,90,60];

	var ime = names[stPacienta-1];
	var priimek = surnames[stPacienta-1];
	var datumRojstva = datesofbirth[stPacienta-1];
	var telesnaVisina = arrayofheights[stPacienta-1];
	var telesnaTeza = arrayofweights[stPacienta-1];
	var sistolicniKrvniTlak = arrayofsbp[stPacienta-1];
	var diastolicniKrvniTlak = arrayofdbp[stPacienta-1];
	var nasicenostKrviSKisikom = arrayofspo2[stPacienta-1];
	var telesnaTemperatura = 0; var datumInUra = "2014-11-21T11:40Z";

		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
			async: false,
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
					async: false,
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
						
						
		                if (party.action == 'CREATE') {
							var dodatek; 
							/*
							'<option value=""></option>'+
							'<option value="'+id1+'">'+ime1+' '+priimek1+'</option>'+
							'<option value="'+id2+'">'+ime2+' '+priimek2+'</option>'+
							'<option value="'+id3+'">'+ime3+' '+priimek3+'</option>';
							$("#preberiObstojeciEHR").html(dodatek);
							*/
							console.log("Muva sta tu, ges pa tij CREATE?");
							if (stPacienta == 1) {
								dodatek = '<option value=""></option><option value="'+ehrId+'">'+ime+' '+priimek+'</option>';
								$("#preberiObstojeciEHR").html(dodatek);
								console.log("Pacient 1");
							} else if (stPacienta == 2) {
								dodatek = '<option value="'+ehrId+'">'+ime+' '+priimek+'</option>';
								$("#preberiObstojeciEHR").append(dodatek);
							} else if (stPacienta == 3) {
								dodatek = '<option value="'+ehrId+'">'+ime+' '+priimek+'</option>';
								$("#preberiObstojeciEHR").append(dodatek);
							}
		                }
						console.log("Imamo id: "+ehrId);
						
						$.ajaxSetup({
							headers: {"Ehr-Session": sessionId}
						});
						
						var podatki = {
							// Struktura predloge je na voljo na naslednjem spletnem naslovu:
							// https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
							"ctx/language": "en",
							"ctx/territory": "SI",
							"ctx/time": datumInUra,
							"vital_signs/height_length/any_event/body_height_length": telesnaVisina,
							"vital_signs/body_weight/any_event/body_weight": telesnaTeza,
							"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
							"vital_signs/body_temperature/any_event/temperature|unit": "°C",
							"vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
							"vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
							"vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
						};
						var parametriZahteve = {
							ehrId: ehrId,
							templateId: 'Vital Signs',
							format: 'FLAT',
							committer: 'Peter Starič'
						};
						
						$.ajax({
							url: baseUrl + "/composition?" + $.param(parametriZahteve),
							type: 'POST',
							async: false,
							contentType: 'application/json',
							data: JSON.stringify(podatki),
							success: function (res) {
								//$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-success fade-in'>" + res.meta.href + ".</span>");
								console.log("Uspelo, podatki naloženi");
								console.log(podatki);
							},
							error: function(err) {
								//$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" +JSON.parse(err.responseText).userMessage + "'!");
								console.log("Ni uspelo");
							}
						});
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	
		
	
	///////////////////////////////
	// TODO: Potrebno implementirati
	return ehrId;
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

/**
 * Kreiraj nov EHR zapis za pacienta in dodaj osnovne demografske podatke.
 * V primeru uspešne akcije izpiši sporočilo s pridobljenim EHR ID, sicer
 * izpiši napako.
 */
function kreirajEHRzaBolnika() {
	var sessionId = getSessionId();
	console.log("Sejaaa: "+sessionId);
	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
	var datumRojstva = $("#kreirajDatumRojstva").val() + "T00:00:00.000Z";
	var telesnaVisina = $("#kreirajVitalnoTelesnaVisina").val();
	var telesnaTeza = $("#kreirajVitalnoTelesnaTeza").val();
	var sistolicniKrvniTlak = $("#kreirajVitalnoKrvniTlakSistolicni").val();
	var diastolicniKrvniTlak = $("#kreirajVitalnoKrvniTlakDiastolicni").val();
	var nasicenostKrviSKisikom = $("#kreirajVitalnoNasicenostKrviSKisikom").val();
	var telesnaTemperatura = 0; var datumInUra = "2014-11-21T11:40Z";
	
	if (!ime || !priimek || !datumRojstva || !telesnaVisina || !telesnaTeza || !sistolicniKrvniTlak || !diastolicniKrvniTlak || !nasicenostKrviSKisikom 
		|| ime.trim().length == 0 || priimek.trim().length == 0 || datumRojstva.trim().length == 0 || telesnaVisina.trim().length == 0 || telesnaTeza.trim().length == 0 
		|| sistolicniKrvniTlak.trim().length == 0 || diastolicniKrvniTlak.trim().length == 0 || nasicenostKrviSKisikom.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
						
						
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#preberiEHRid").val(ehrId);
		                }
						
						$.ajaxSetup({
							headers: {"Ehr-Session": sessionId}
						});
						
						var podatki = {
							// Struktura predloge je na voljo na naslednjem spletnem naslovu:
							// https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
							"ctx/language": "en",
							"ctx/territory": "SI",
							"ctx/time": datumInUra,
							"vital_signs/height_length/any_event/body_height_length": telesnaVisina,
							"vital_signs/body_weight/any_event/body_weight": telesnaTeza,
							"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
							"vital_signs/body_temperature/any_event/temperature|unit": "°C",
							"vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
							"vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
							"vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
						};
						var parametriZahteve = {
							ehrId: ehrId,
							templateId: 'Vital Signs',
							format: 'FLAT',
							committer: 'Peter Starič'
						};
						
						$.ajax({
							url: baseUrl + "/composition?" + $.param(parametriZahteve),
							type: 'POST',
							contentType: 'application/json',
							data: JSON.stringify(podatki),
							success: function (res) {
								//$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-success fade-in'>" + res.meta.href + ".</span>");
								console.log("Uspelo, podatki naloženi");
							},
							error: function(err) {
								//$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" +JSON.parse(err.responseText).userMessage + "'!");
								console.log("Ni uspelo");
							}
						});
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
}


/**
 * Za podan EHR ID preberi demografske podrobnosti pacienta in izpiši sporočilo
 * s pridobljenimi podatki (ime, priimek in datum rojstva).
 */
function preberiEHRodBolnika() {
	sessionId = getSessionId();

	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				var ddd = new Date(party.dateOfBirth);
				var diff_ms = Date.now() - ddd.getTime();
				var age_dt = new Date(diff_ms); 
				var starost = Math.abs(age_dt.getUTCFullYear() - 1970);
				console.log("Staros je: "+starost);
				$("#imeOsebe").html(party.firstNames);
				$("#priimekOsebe").html(party.lastNames);
				$("#starostOsebe").html(starost);
			},
			error: function(err) {
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
			}
		});
		
		//tule dobimo visino pa to
		var telesnaVisina;
		var telesnaTeza;
		var sistolicniKrvniTlak;
		var diastolicniKrvniTlak;
		var nasicenostKrviSKisikom;
		var osebaStarost;
		$.when(
			$.ajax({
				url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
				type: 'GET',
				headers: {"Ehr-Session": sessionId},
	    		success: function (data) {
					var party = data.party;
					var ddd = new Date(party.dateOfBirth);
					var diff_ms = Date.now() - ddd.getTime();
					var age_dt = new Date(diff_ms); 
					osebaStarost = Math.abs(age_dt.getUTCFullYear() - 1970);
				},
				error: function(err) {
					$("#preberiSporocilo").html("<span class='obvestilo label " +
        			  "label-danger fade-in'>Napaka '" +
        			JSON.parse(err.responseText).userMessage + "'!");
				}
			}),
			$.ajax({
				url: baseUrl + "/view/" + ehrId + "/" + "height",
				type: 'GET',
				headers: {"Ehr-Session": sessionId},
				success: function (res) {
					telesnaVisina = res[0].height;
				},
				error: function() {
					$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" +JSON.parse(err.responseText).userMessage + "'!");
				}
			}),	
			$.ajax({
				url: baseUrl + "/view/" + ehrId + "/" + "weight",
				type: 'GET',
				headers: {"Ehr-Session": sessionId},
				success: function (res) {
					telesnaTeza = res[0].weight;
				},
				error: function() {
					$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" +JSON.parse(err.responseText).userMessage + "'!");
				}
			}),
			$.ajax({
				url: baseUrl + "/view/" + ehrId + "/" + "blood_pressure",
				type: 'GET',
				headers: {"Ehr-Session": sessionId},
				success: function (res) {
					sistolicniKrvniTlak = res[0].systolic;
					diastolicniKrvniTlak = res[0].diastolic;	
				},
				error: function() {
					$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" +JSON.parse(err.responseText).userMessage + "'!");
				}
			}),
			$.ajax({
				url: baseUrl + "/view/" + ehrId + "/" + "spO2",
				type: 'GET',
				headers: {"Ehr-Session": sessionId},
				success: function (res) {
					nasicenostKrviSKisikom = res[0].spO2;
				},
				error: function() {
					$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" +JSON.parse(err.responseText).userMessage + "'!");
				}
			})
		).then(function() {
			/*console.log("Višina: "+telesnaVisina);
			console.log("Teza "+telesnaTeza);
			console.log("DKT: "+sistolicniKrvniTlak);
			console.log("SKT: "+diastolicniKrvniTlak);
			console.log("NKSK: "+nasicenostKrviSKisikom);*/
			var bmi1 = (telesnaTeza/((telesnaVisina/100)*(telesnaVisina/100)));
			var bmi2 = Math.round((bmi1 + 0.00001) * 100) / 100
			$("#bmiOsebe").html(bmi2);
			$("#sktOsebe").html(sistolicniKrvniTlak);
			$("#dktOsebe").html(diastolicniKrvniTlak);
			$("#nkskOsebe").html(nasicenostKrviSKisikom);
			//za BMI
			if (bmi2 < 18.50) {
			    $("#rezultatBMI").html('<div class="alert alert-warning">Imate <b>nizko</b> vrednost indeksa telesne mase. Tveganje oz. težave, ki lahko nastopijo: '+'<ul style="list-style-type:disc"><li>zmanjšanje gostote kostne mase</li><li>zmanjšanje odpornosti imunskega sistema</li><li>nastanek srčnih nepravilnosti</li><li>pomanjkanje železa v krvi</li></ul></div>');
			}
			else if (bmi2 >= 18.50 && bmi2 <= 24.99) {
			    $("#rezultatBMI").html('<div class="alert alert-success">Imate <b>normalno</b> vrednost indeksa telesne mase. Tveganje oz. težave, ki lahko nastopijo: /</div>');
			}
			else if (bmi2 > 25) {
				$("#rezultatBMI").html('<div class="alert alert-danger">Imate <b>povišano</b> vrednost indeksa telesne mase. Tveganje oz. težave, ki lahko nastopijo:'+'<ul><li>srčno-žilne bolezni</li><li>dislipidimije</li><li>tipa 2 sladkorne bolezni</li><li>bolezni žolčnika</li><li>artroza (obrabe sklepov)</li><li>sindrom apneje (motnje dihanja v spanju)</li><li>kap</li><li>vsaj 10 vrst raka</li></ul></div>');
			}
			//za pritisk
			if ((sistolicniKrvniTlak >= 140 || diastolicniKrvniTlak >= 90) && (osebaStarost >= 40 && osebaStarost <= 69)) {
				$("#rezultatPritiska").html('<div class="alert alert-danger">Imate <b>visok</b> krvni tlak.Tveganje oz. težave, ki lahko nastopijo:'+'<ul><li>izguba vida</li><li>kap</li><li>odpoved srca</li><li>srčni napad</li><li>odpoved/bolezen ledvic</li></ul></div>');
			}
			else if (osebaStarost < 40 || osebaStarost > 69){
				$("#rezultatPritiska").html('<div class="alert alert-success">Spadate v starostno skupino za katero krvni tlak ne predstavlja veliko razliko pri tveganjih. (Vendar ta naraščajo z zviševanjem le-tega.)</div>');
			}
			else {
				$("#rezultatPritiska").html('<div class="alert alert-success">Imate <b>normalen</b> krvni tlak. Tveganje oz. težave, ki lahko nastopijo: /</div>');
			}
			//za NKSK
			if (nasicenostKrviSKisikom >= 95) {
				$("#rezultatNKSK").html('<div class="alert alert-success">Imate <b>normalno</b> raven nasičenosti krvi s kisikom. Tveganje oz. težave, ki lahko nastopijo: /');
			}
			else {
				$("#rezultatNKSK").html('<div class="alert alert-danger">Imate <b>nizko</b> raven nasičenosti krvi s kisikom. Tveganje oz. težave, ki lahko nastopijo: '+'<ul><li>pomankanje sape</li><li>bolečina v prsih</li><li>zmedenost</li><li>glavobol</li><li>cianoza</li><li>hiter srčni utrip</li></ul></div>');
			}
		});	
	}
}

//prostor za podatke
var pozicija = [0,30,60,40,42,121,150,180];
var seznamPodatkov = [];
function pridobiPodatke(podatki) {
	for (var i = 0; i < pozicija.length; i++) {
		console.log("Data: "+podatki.fact[pozicija[i]].Value);
		seznamPodatkov.push(podatki.fact[pozicija[i]].Value);
	}
}
pridobiPodatke(whoData);
narisiGraf();

function narisiGraf() {

	//usage:
	console.log("LALALA");
	console.log("Preko regdata: "+whoData.fact[0].Value);	

    google.charts.load('current', {'packages':['corechart']});

    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
      // Create the data table.
      var data = new google.visualization.DataTable();
      data.addColumn('string', 'WHO regija');
      data.addColumn('number', 'Leta v katerih je oseba (popolnoma) zdrava');
      data.addRows([
        ['Afrika', parseFloat(seznamPodatkov[0])],
        ['Ameriki', parseFloat(seznamPodatkov[1])],
        ['Jugovzhodna Azija', parseFloat(seznamPodatkov[2])], 
        ['Evropa', parseFloat(seznamPodatkov[3])],
        ['Vzhodnji Mediteran', parseFloat(seznamPodatkov[4])],
        ['Zahodnji Pacifik', parseFloat(seznamPodatkov[5])],
        ['Globalna regija', parseFloat(seznamPodatkov[6])]
      ]);
      // Set chart options
	  var sirina = $("#chart_div").width();
	  console.log(sirina);
      var options = {'title':'Podatki glede na regije WHO (World Health Organization)',
                     'width':sirina,
                     'height':300};

      // Instantiate and draw our chart, passing in some options.
      var chart = new google.visualization.ColumnChart(document.getElementById("chart_div"));
      chart.draw(data, options);
	
    }
}
function resizeGraf () {
		narisiGraf();
}
function narisiTabelo(stevilka) {
	
	if (stevilka == 1) {
		$("#tabelaPrednosti").html('<table class="table table-bordered" style="width:30%; float: right;"><thead><tr><th>Prednosti</th></tr></thead><tbody><tr><td>poveča raven energije</td></tr><tr><td>spodbuja delovanje sivih celic</td></tr><tr><td>zmanjšuje verjetnost kroničnih obolenj</td></tr><tr><td>vpliva na lepšo kožo</td> </tr><tr><td>zmanjšuje verjetnost rakavih obolenj</td></tr></tbody></table>');
	}
	else if (stevilka == 2) {
		$("#tabelaPrednosti").html('<table class="table table-bordered" style="width:30%; float: right;"><thead><tr><th>Prednosti</th></tr></thead><tbody><tr><td>zmanjšuje tveganje kapi, srčnega napada, diabetesa</td></tr><tr><td>izboljša stabilnost sklepov</td></tr><tr><td>zmanjšuje verjetnost kroničnih obolenj</td></tr><tr><td>vzdržuje kostno maso</td></tr><tr><td>krepi samozavest</td></tr><tr><td>zmanjšuje stres</tr></tbody></table>');
	}
	else if (stevilka == 3) {
		$("#tabelaPrednosti").html('<table class="table table-bordered" style="width:30%; float: right;"><thead><tr><th>Prednosti</th></tr></thead><tbody><tr><td>izboljšuje spomin</td></tr><tr><td>zmanjšuje krvni tlak</td></tr><tr><td>zmanjšuje bolečino</td></tr><tr><td>vzdržuje telesno težo</td></tr><tr><td>boljše razpoloženje</td></tr></tbody></table>');
	}
	else if (stevilka == 4) {
		$("#tabelaPrednosti").html('<table class="table table-bordered" style="width:30%; float: right;"><thead><tr><th>Prednosti</th></tr></thead><tbody><tr><td>zmanjšuje izčrpanost</td></tr><tr><td>boljši vzorci spanja</td></tr><tr><td>sprošča mišice</td></tr><tr><td>varovanje pred depresijo</td></tr><tr><td>zmanjšuje možnost prehladitve</td></tr></tbody></table>');
	}
	else if (stevilka == 5) {
		$("#tabelaPrednosti").html('<table class="table table-bordered" style="width:30%; float: right;"><thead><tr><th>Prednosti</th></tr></thead><tbody><tr><td>daljša življenska doba</td></tr><tr><td>spodbuja zdrave navade</td></tr><tr><td>zmanjša tveganje razvoja Alzhaimerjeve bolezni</td></tr><tr><td>izboljša imunski sistem</td></tr><tr><td>zmanjšuje stres</td></tr></tbody></table>');
	}
}

$(document).ready(function() {

  /**
   * Napolni testne vrednosti (ime, priimek in datum rojstva) pri kreiranju
   * EHR zapisa za novega bolnika, ko uporabnik izbere vrednost iz
   * padajočega menuja (npr. Pujsa Pepa).
   */
  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(",");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#kreirajDatumRojstva").val(podatki[2]);
    $("#kreirajVitalnoTelesnaVisina").val(podatki[3]);
    $("#kreirajVitalnoTelesnaTeza").val(podatki[4]);
    $("#kreirajVitalnoKrvniTlakSistolicni").val(podatki[5]);
    $("#kreirajVitalnoKrvniTlakDiastolicni").val(podatki[6]);
    $("#kreirajVitalnoNasicenostKrviSKisikom").val(podatki[7]);
  });

  /**
   * Napolni testni EHR ID pri prebiranju EHR zapisa obstoječega bolnika,
   * ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Dejan Lavbič, Pujsa Pepa, Ata Smrk)
   */
	$('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
	});

  /**
   * Napolni testne vrednosti (EHR ID, datum in ura, telesna višina,
   * telesna teža, telesna temperatura, sistolični in diastolični krvni tlak,
   * nasičenost krvi s kisikom in merilec) pri vnosu meritve vitalnih znakov
   * bolnika, ko uporabnik izbere vrednosti iz padajočega menuja (npr. Ata Smrk)
   */
	$('#preberiObstojeciVitalniZnak').change(function() {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("");
		var podatki = $(this).val().split("|");
		$("#dodajVitalnoEHR").val(podatki[0]);
		$("#dodajVitalnoDatumInUra").val(podatki[1]);
		$("#dodajVitalnoTelesnaVisina").val(podatki[2]);
		$("#dodajVitalnoTelesnaTeza").val(podatki[3]);
		$("#dodajVitalnoTelesnaTemperatura").val(podatki[4]);
		$("#dodajVitalnoKrvniTlakSistolicni").val(podatki[5]);
		$("#dodajVitalnoKrvniTlakDiastolicni").val(podatki[6]);
		$("#dodajVitalnoNasicenostKrviSKisikom").val(podatki[7]);
		$("#dodajVitalnoMerilec").val(podatki[8]);
	});

  /**
   * Napolni testni EHR ID pri pregledu meritev vitalnih znakov obstoječega
   * bolnika, ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Ata Smrk, Pujsa Pepa)
   */
	$('#preberiEhrIdZaVitalneZnake').change(function() {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("");
		$("#rezultatMeritveVitalnihZnakov").html("");
		$("#meritveVitalnihZnakovEHRid").val($(this).val());
	});
	narisiGraf();
	window.onload = resizeGraf;
	window.onresize = resizeGraf;
});
