var whoData = {
  "dimension": [
                 {
                   "label": "GHO",
                   "display": "Indicator"
                 },
                 {
                   "label": "PUBLISHSTATE",
                   "display": "PUBLISH STATES"
                 },
                 {
                   "label": "YEAR",
                   "display": "Year"
                 },
                 {
                   "label": "REGION",
                   "display": "WHO region"
                 },
                 {
                   "label": "SEX",
                   "display": "Sex"
                 }
               ],
    "fact": [
              {
                "dims": {
                          "REGION": "Africa",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2016"
                        },
                "Value": "53.8"
              },
              {
                "dims": {
                          "REGION": "Africa",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2015"
                        },
                "Value": "53.3"
              },
              {
                "dims": {
                          "REGION": "Africa",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2010"
                        },
                "Value": "50.4"
              },
              {
                "dims": {
                          "REGION": "Africa",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2005"
                        },
                "Value": "46.7"
              },
              {
                "dims": {
                          "REGION": "Africa",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2000"
                        },
                "Value": "44.4"
              },
              {
                "dims": {
                          "REGION": "Africa",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Male",
                          "YEAR": "2016"
                        },
                "Value": "52.6"
              },
              {
                "dims": {
                          "REGION": "Africa",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Male",
                          "YEAR": "2015"
                        },
                "Value": "52.1"
              },
              {
                "dims": {
                          "REGION": "Africa",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Male",
                          "YEAR": "2010"
                        },
                "Value": "49.6"
              },
              {
                "dims": {
                          "REGION": "Africa",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Male",
                          "YEAR": "2005"
                        },
                "Value": "46.0"
              },
              {
                "dims": {
                          "REGION": "Africa",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Male",
                          "YEAR": "2000"
                        },
                "Value": "43.5"
              },
              {
                "dims": {
                          "REGION": "Africa",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Female",
                          "YEAR": "2016"
                        },
                "Value": "54.9"
              },
              {
                "dims": {
                          "REGION": "Africa",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Female",
                          "YEAR": "2015"
                        },
                "Value": "54.4"
              },
              {
                "dims": {
                          "REGION": "Africa",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Female",
                          "YEAR": "2010"
                        },
                "Value": "51.3"
              },
              {
                "dims": {
                          "REGION": "Africa",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Female",
                          "YEAR": "2005"
                        },
                "Value": "47.4"
              },
              {
                "dims": {
                          "REGION": "Africa",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Female",
                          "YEAR": "2000"
                        },
                "Value": "45.3"
              },
              {
                "dims": {
                          "REGION": "Africa",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2016"
                        },
                "Value": "12.5"
              },
              {
                "dims": {
                          "REGION": "Africa",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2015"
                        },
                "Value": "12.4"
              },
              {
                "dims": {
                          "REGION": "Africa",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2010"
                        },
                "Value": "12.0"
              },
              {
                "dims": {
                          "REGION": "Africa",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2005"
                        },
                "Value": "11.4"
              },
              {
                "dims": {
                          "REGION": "Africa",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2000"
                        },
                "Value": "11.0"
              },
              {
                "dims": {
                          "REGION": "Africa",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Male",
                          "YEAR": "2016"
                        },
                "Value": "12.0"
              },
              {
                "dims": {
                          "REGION": "Africa",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Male",
                          "YEAR": "2015"
                        },
                "Value": "11.8"
              },
              {
                "dims": {
                          "REGION": "Africa",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Male",
                          "YEAR": "2010"
                        },
                "Value": "11.5"
              },
              {
                "dims": {
                          "REGION": "Africa",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Male",
                          "YEAR": "2005"
                        },
                "Value": "10.9"
              },
              {
                "dims": {
                          "REGION": "Africa",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Male",
                          "YEAR": "2000"
                        },
                "Value": "10.5"
              },
              {
                "dims": {
                          "REGION": "Africa",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Female",
                          "YEAR": "2016"
                        },
                "Value": "13.1"
              },
              {
                "dims": {
                          "REGION": "Africa",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Female",
                          "YEAR": "2015"
                        },
                "Value": "13.0"
              },
              {
                "dims": {
                          "REGION": "Africa",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Female",
                          "YEAR": "2010"
                        },
                "Value": "12.4"
              },
              {
                "dims": {
                          "REGION": "Africa",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Female",
                          "YEAR": "2005"
                        },
                "Value": "11.8"
              },
              {
                "dims": {
                          "REGION": "Africa",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Female",
                          "YEAR": "2000"
                        },
                "Value": "11.4"
              },
              {
                "dims": {
                          "REGION": "Americas",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2016"
                        },
                "Value": "67.5"
              },
              {
                "dims": {
                          "REGION": "Americas",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2015"
                        },
                "Value": "67.4"
              },
              {
                "dims": {
                          "REGION": "Americas",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2010"
                        },
                "Value": "66.3"
              },
              {
                "dims": {
                          "REGION": "Americas",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2005"
                        },
                "Value": "66.0"
              },
              {
                "dims": {
                          "REGION": "Americas",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2000"
                        },
                "Value": "64.9"
              },
              {
                "dims": {
                          "REGION": "Americas",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Male",
                          "YEAR": "2016"
                        },
                "Value": "65.5"
              },
              {
                "dims": {
                          "REGION": "Americas",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Male",
                          "YEAR": "2015"
                        },
                "Value": "65.3"
              },
              {
                "dims": {
                          "REGION": "Americas",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Male",
                          "YEAR": "2010"
                        },
                "Value": "64.1"
              },
              {
                "dims": {
                          "REGION": "Americas",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Male",
                          "YEAR": "2005"
                        },
                "Value": "63.8"
              },
              {
                "dims": {
                          "REGION": "Americas",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Male",
                          "YEAR": "2000"
                        },
                "Value": "62.6"
              },
              {
                "dims": {
                          "REGION": "Americas",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Female",
                          "YEAR": "2016"
                        },
                "Value": "69.6"
              },
              {
                "dims": {
                          "REGION": "Americas",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Female",
                          "YEAR": "2015"
                        },
                "Value": "69.5"
              },
              {
                "dims": {
                          "REGION": "Americas",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Female",
                          "YEAR": "2010"
                        },
                "Value": "68.4"
              },
              {
                "dims": {
                          "REGION": "Americas",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Female",
                          "YEAR": "2005"
                        },
                "Value": "68.1"
              },
              {
                "dims": {
                          "REGION": "Americas",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Female",
                          "YEAR": "2000"
                        },
                "Value": "67.1"
              },
              {
                "dims": {
                          "REGION": "Americas",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2016"
                        },
                "Value": "17.6"
              },
              {
                "dims": {
                          "REGION": "Americas",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2015"
                        },
                "Value": "17.5"
              },
              {
                "dims": {
                          "REGION": "Americas",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2010"
                        },
                "Value": "17.0"
              },
              {
                "dims": {
                          "REGION": "Americas",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2005"
                        },
                "Value": "16.6"
              },
              {
                "dims": {
                          "REGION": "Americas",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2000"
                        },
                "Value": "16.2"
              },
              {
                "dims": {
                          "REGION": "Americas",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Male",
                          "YEAR": "2016"
                        },
                "Value": "16.4"
              },
              {
                "dims": {
                          "REGION": "Americas",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Male",
                          "YEAR": "2015"
                        },
                "Value": "16.2"
              },
              {
                "dims": {
                          "REGION": "Americas",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Male",
                          "YEAR": "2010"
                        },
                "Value": "15.8"
              },
              {
                "dims": {
                          "REGION": "Americas",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Male",
                          "YEAR": "2005"
                        },
                "Value": "15.3"
              },
              {
                "dims": {
                          "REGION": "Americas",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Male",
                          "YEAR": "2000"
                        },
                "Value": "14.8"
              },
              {
                "dims": {
                          "REGION": "Americas",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Female",
                          "YEAR": "2016"
                        },
                "Value": "18.7"
              },
              {
                "dims": {
                          "REGION": "Americas",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Female",
                          "YEAR": "2015"
                        },
                "Value": "18.6"
              },
              {
                "dims": {
                          "REGION": "Americas",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Female",
                          "YEAR": "2010"
                        },
                "Value": "18.2"
              },
              {
                "dims": {
                          "REGION": "Americas",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Female",
                          "YEAR": "2005"
                        },
                "Value": "17.8"
              },
              {
                "dims": {
                          "REGION": "Americas",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Female",
                          "YEAR": "2000"
                        },
                "Value": "17.4"
              },
              {
                "dims": {
                          "REGION": "South-East Asia",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2016"
                        },
                "Value": "60.4"
              },
              {
                "dims": {
                          "REGION": "South-East Asia",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2015"
                        },
                "Value": "60.0"
              },
              {
                "dims": {
                          "REGION": "South-East Asia",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2010"
                        },
                "Value": "58.5"
              },
              {
                "dims": {
                          "REGION": "South-East Asia",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2005"
                        },
                "Value": "56.6"
              },
              {
                "dims": {
                          "REGION": "South-East Asia",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2000"
                        },
                "Value": "54.8"
              },
              {
                "dims": {
                          "REGION": "South-East Asia",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Male",
                          "YEAR": "2016"
                        },
                "Value": "59.5"
              },
              {
                "dims": {
                          "REGION": "South-East Asia",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Male",
                          "YEAR": "2015"
                        },
                "Value": "59.2"
              },
              {
                "dims": {
                          "REGION": "South-East Asia",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Male",
                          "YEAR": "2010"
                        },
                "Value": "57.9"
              },
              {
                "dims": {
                          "REGION": "South-East Asia",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Male",
                          "YEAR": "2005"
                        },
                "Value": "56.3"
              },
              {
                "dims": {
                          "REGION": "South-East Asia",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Male",
                          "YEAR": "2000"
                        },
                "Value": "54.5"
              },
              {
                "dims": {
                          "REGION": "South-East Asia",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Female",
                          "YEAR": "2016"
                        },
                "Value": "61.3"
              },
              {
                "dims": {
                          "REGION": "South-East Asia",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Female",
                          "YEAR": "2015"
                        },
                "Value": "60.9"
              },
              {
                "dims": {
                          "REGION": "South-East Asia",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Female",
                          "YEAR": "2010"
                        },
                "Value": "59.0"
              },
              {
                "dims": {
                          "REGION": "South-East Asia",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Female",
                          "YEAR": "2005"
                        },
                "Value": "56.9"
              },
              {
                "dims": {
                          "REGION": "South-East Asia",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Female",
                          "YEAR": "2000"
                        },
                "Value": "55.1"
              },
              {
                "dims": {
                          "REGION": "South-East Asia",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2016"
                        },
                "Value": "13.3"
              },
              {
                "dims": {
                          "REGION": "South-East Asia",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2015"
                        },
                "Value": "13.2"
              },
              {
                "dims": {
                          "REGION": "South-East Asia",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2010"
                        },
                "Value": "12.7"
              },
              {
                "dims": {
                          "REGION": "South-East Asia",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2005"
                        },
                "Value": "12.2"
              },
              {
                "dims": {
                          "REGION": "South-East Asia",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2000"
                        },
                "Value": "11.9"
              },
              {
                "dims": {
                          "REGION": "South-East Asia",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Male",
                          "YEAR": "2016"
                        },
                "Value": "12.7"
              },
              {
                "dims": {
                          "REGION": "South-East Asia",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Male",
                          "YEAR": "2015"
                        },
                "Value": "12.6"
              },
              {
                "dims": {
                          "REGION": "South-East Asia",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Male",
                          "YEAR": "2010"
                        },
                "Value": "12.2"
              },
              {
                "dims": {
                          "REGION": "South-East Asia",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Male",
                          "YEAR": "2005"
                        },
                "Value": "11.8"
              },
              {
                "dims": {
                          "REGION": "South-East Asia",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Male",
                          "YEAR": "2000"
                        },
                "Value": "11.4"
              },
              {
                "dims": {
                          "REGION": "South-East Asia",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Female",
                          "YEAR": "2016"
                        },
                "Value": "13.9"
              },
              {
                "dims": {
                          "REGION": "South-East Asia",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Female",
                          "YEAR": "2015"
                        },
                "Value": "13.7"
              },
              {
                "dims": {
                          "REGION": "South-East Asia",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Female",
                          "YEAR": "2010"
                        },
                "Value": "13.2"
              },
              {
                "dims": {
                          "REGION": "South-East Asia",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Female",
                          "YEAR": "2005"
                        },
                "Value": "12.7"
              },
              {
                "dims": {
                          "REGION": "South-East Asia",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Female",
                          "YEAR": "2000"
                        },
                "Value": "12.4"
              },
              {
                "dims": {
                          "REGION": "Europe",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2016"
                        },
                "Value": "68.4"
              },
              {
                "dims": {
                          "REGION": "Europe",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2015"
                        },
                "Value": "68.1"
              },
              {
                "dims": {
                          "REGION": "Europe",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2010"
                        },
                "Value": "66.9"
              },
              {
                "dims": {
                          "REGION": "Europe",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2005"
                        },
                "Value": "65.1"
              },
              {
                "dims": {
                          "REGION": "Europe",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2000"
                        },
                "Value": "64.2"
              },
              {
                "dims": {
                          "REGION": "Europe",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Male",
                          "YEAR": "2016"
                        },
                "Value": "66.1"
              },
              {
                "dims": {
                          "REGION": "Europe",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Male",
                          "YEAR": "2015"
                        },
                "Value": "65.7"
              },
              {
                "dims": {
                          "REGION": "Europe",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Male",
                          "YEAR": "2010"
                        },
                "Value": "64.3"
              },
              {
                "dims": {
                          "REGION": "Europe",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Male",
                          "YEAR": "2005"
                        },
                "Value": "62.2"
              },
              {
                "dims": {
                          "REGION": "Europe",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Male",
                          "YEAR": "2000"
                        },
                "Value": "61.2"
              },
              {
                "dims": {
                          "REGION": "Europe",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Female",
                          "YEAR": "2016"
                        },
                "Value": "70.7"
              },
              {
                "dims": {
                          "REGION": "Europe",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Female",
                          "YEAR": "2015"
                        },
                "Value": "70.5"
              },
              {
                "dims": {
                          "REGION": "Europe",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Female",
                          "YEAR": "2010"
                        },
                "Value": "69.5"
              },
              {
                "dims": {
                          "REGION": "Europe",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Female",
                          "YEAR": "2005"
                        },
                "Value": "68.2"
              },
              {
                "dims": {
                          "REGION": "Europe",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Female",
                          "YEAR": "2000"
                        },
                "Value": "67.3"
              },
              {
                "dims": {
                          "REGION": "Europe",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2016"
                        },
                "Value": "17.4"
              },
              {
                "dims": {
                          "REGION": "Europe",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2015"
                        },
                "Value": "17.2"
              },
              {
                "dims": {
                          "REGION": "Europe",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2010"
                        },
                "Value": "16.7"
              },
              {
                "dims": {
                          "REGION": "Europe",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2005"
                        },
                "Value": "15.8"
              },
              {
                "dims": {
                          "REGION": "Europe",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2000"
                        },
                "Value": "15.3"
              },
              {
                "dims": {
                          "REGION": "Europe",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Male",
                          "YEAR": "2016"
                        },
                "Value": "15.9"
              },
              {
                "dims": {
                          "REGION": "Europe",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Male",
                          "YEAR": "2015"
                        },
                "Value": "15.7"
              },
              {
                "dims": {
                          "REGION": "Europe",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Male",
                          "YEAR": "2010"
                        },
                "Value": "15.1"
              },
              {
                "dims": {
                          "REGION": "Europe",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Male",
                          "YEAR": "2005"
                        },
                "Value": "14.2"
              },
              {
                "dims": {
                          "REGION": "Europe",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Male",
                          "YEAR": "2000"
                        },
                "Value": "13.5"
              },
              {
                "dims": {
                          "REGION": "Europe",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Female",
                          "YEAR": "2016"
                        },
                "Value": "18.7"
              },
              {
                "dims": {
                          "REGION": "Europe",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Female",
                          "YEAR": "2015"
                        },
                "Value": "18.5"
              },
              {
                "dims": {
                          "REGION": "Europe",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Female",
                          "YEAR": "2010"
                        },
                "Value": "18.0"
              },
              {
                "dims": {
                          "REGION": "Europe",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Female",
                          "YEAR": "2005"
                        },
                "Value": "17.3"
              },
              {
                "dims": {
                          "REGION": "Europe",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Female",
                          "YEAR": "2000"
                        },
                "Value": "16.7"
              },
              {
                "dims": {
                          "REGION": "Eastern Mediterranean",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2016"
                        },
                "Value": "59.7"
              },
              {
                "dims": {
                          "REGION": "Eastern Mediterranean",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2015"
                        },
                "Value": "59.4"
              },
              {
                "dims": {
                          "REGION": "Eastern Mediterranean",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2010"
                        },
                "Value": "58.6"
              },
              {
                "dims": {
                          "REGION": "Eastern Mediterranean",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2005"
                        },
                "Value": "57.0"
              },
              {
                "dims": {
                          "REGION": "Eastern Mediterranean",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2000"
                        },
                "Value": "56.2"
              },
              {
                "dims": {
                          "REGION": "Eastern Mediterranean",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Male",
                          "YEAR": "2016"
                        },
                "Value": "59.1"
              },
              {
                "dims": {
                          "REGION": "Eastern Mediterranean",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Male",
                          "YEAR": "2015"
                        },
                "Value": "58.8"
              },
              {
                "dims": {
                          "REGION": "Eastern Mediterranean",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Male",
                          "YEAR": "2010"
                        },
                "Value": "58.1"
              },
              {
                "dims": {
                          "REGION": "Eastern Mediterranean",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Male",
                          "YEAR": "2005"
                        },
                "Value": "56.4"
              },
              {
                "dims": {
                          "REGION": "Eastern Mediterranean",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Male",
                          "YEAR": "2000"
                        },
                "Value": "55.8"
              },
              {
                "dims": {
                          "REGION": "Eastern Mediterranean",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Female",
                          "YEAR": "2016"
                        },
                "Value": "60.4"
              },
              {
                "dims": {
                          "REGION": "Eastern Mediterranean",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Female",
                          "YEAR": "2015"
                        },
                "Value": "60.1"
              },
              {
                "dims": {
                          "REGION": "Eastern Mediterranean",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Female",
                          "YEAR": "2010"
                        },
                "Value": "59.2"
              },
              {
                "dims": {
                          "REGION": "Eastern Mediterranean",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Female",
                          "YEAR": "2005"
                        },
                "Value": "57.6"
              },
              {
                "dims": {
                          "REGION": "Eastern Mediterranean",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Female",
                          "YEAR": "2000"
                        },
                "Value": "56.8"
              },
              {
                "dims": {
                          "REGION": "Eastern Mediterranean",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2016"
                        },
                "Value": "13.3"
              },
              {
                "dims": {
                          "REGION": "Eastern Mediterranean",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2015"
                        },
                "Value": "13.2"
              },
              {
                "dims": {
                          "REGION": "Eastern Mediterranean",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2010"
                        },
                "Value": "13.0"
              },
              {
                "dims": {
                          "REGION": "Eastern Mediterranean",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2005"
                        },
                "Value": "12.6"
              },
              {
                "dims": {
                          "REGION": "Eastern Mediterranean",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2000"
                        },
                "Value": "12.4"
              },
              {
                "dims": {
                          "REGION": "Eastern Mediterranean",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Male",
                          "YEAR": "2016"
                        },
                "Value": "13.0"
              },
              {
                "dims": {
                          "REGION": "Eastern Mediterranean",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Male",
                          "YEAR": "2015"
                        },
                "Value": "12.9"
              },
              {
                "dims": {
                          "REGION": "Eastern Mediterranean",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Male",
                          "YEAR": "2010"
                        },
                "Value": "12.7"
              },
              {
                "dims": {
                          "REGION": "Eastern Mediterranean",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Male",
                          "YEAR": "2005"
                        },
                "Value": "12.3"
              },
              {
                "dims": {
                          "REGION": "Eastern Mediterranean",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Male",
                          "YEAR": "2000"
                        },
                "Value": "12.1"
              },
              {
                "dims": {
                          "REGION": "Eastern Mediterranean",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Female",
                          "YEAR": "2016"
                        },
                "Value": "13.6"
              },
              {
                "dims": {
                          "REGION": "Eastern Mediterranean",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Female",
                          "YEAR": "2015"
                        },
                "Value": "13.5"
              },
              {
                "dims": {
                          "REGION": "Eastern Mediterranean",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Female",
                          "YEAR": "2010"
                        },
                "Value": "13.3"
              },
              {
                "dims": {
                          "REGION": "Eastern Mediterranean",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Female",
                          "YEAR": "2005"
                        },
                "Value": "12.9"
              },
              {
                "dims": {
                          "REGION": "Eastern Mediterranean",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Female",
                          "YEAR": "2000"
                        },
                "Value": "12.7"
              },
              {
                "dims": {
                          "REGION": "Western Pacific",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2016"
                        },
                "Value": "68.9"
              },
              {
                "dims": {
                          "REGION": "Western Pacific",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2015"
                        },
                "Value": "68.6"
              },
              {
                "dims": {
                          "REGION": "Western Pacific",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2010"
                        },
                "Value": "67.8"
              },
              {
                "dims": {
                          "REGION": "Western Pacific",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2005"
                        },
                "Value": "66.9"
              },
              {
                "dims": {
                          "REGION": "Western Pacific",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2000"
                        },
                "Value": "65.2"
              },
              {
                "dims": {
                          "REGION": "Western Pacific",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Male",
                          "YEAR": "2016"
                        },
                "Value": "67.7"
              },
              {
                "dims": {
                          "REGION": "Western Pacific",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Male",
                          "YEAR": "2015"
                        },
                "Value": "67.5"
              },
              {
                "dims": {
                          "REGION": "Western Pacific",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Male",
                          "YEAR": "2010"
                        },
                "Value": "66.7"
              },
              {
                "dims": {
                          "REGION": "Western Pacific",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Male",
                          "YEAR": "2005"
                        },
                "Value": "65.7"
              },
              {
                "dims": {
                          "REGION": "Western Pacific",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Male",
                          "YEAR": "2000"
                        },
                "Value": "64.0"
              },
              {
                "dims": {
                          "REGION": "Western Pacific",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Female",
                          "YEAR": "2016"
                        },
                "Value": "70.0"
              },
              {
                "dims": {
                          "REGION": "Western Pacific",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Female",
                          "YEAR": "2015"
                        },
                "Value": "69.8"
              },
              {
                "dims": {
                          "REGION": "Western Pacific",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Female",
                          "YEAR": "2010"
                        },
                "Value": "69.1"
              },
              {
                "dims": {
                          "REGION": "Western Pacific",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Female",
                          "YEAR": "2005"
                        },
                "Value": "68.1"
              },
              {
                "dims": {
                          "REGION": "Western Pacific",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Female",
                          "YEAR": "2000"
                        },
                "Value": "66.5"
              },
              {
                "dims": {
                          "REGION": "Western Pacific",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2016"
                        },
                "Value": "16.6"
              },
              {
                "dims": {
                          "REGION": "Western Pacific",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2015"
                        },
                "Value": "16.5"
              },
              {
                "dims": {
                          "REGION": "Western Pacific",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2010"
                        },
                "Value": "16.2"
              },
              {
                "dims": {
                          "REGION": "Western Pacific",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2005"
                        },
                "Value": "15.9"
              },
              {
                "dims": {
                          "REGION": "Western Pacific",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2000"
                        },
                "Value": "15.3"
              },
              {
                "dims": {
                          "REGION": "Western Pacific",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Male",
                          "YEAR": "2016"
                        },
                "Value": "15.6"
              },
              {
                "dims": {
                          "REGION": "Western Pacific",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Male",
                          "YEAR": "2015"
                        },
                "Value": "15.5"
              },
              {
                "dims": {
                          "REGION": "Western Pacific",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Male",
                          "YEAR": "2010"
                        },
                "Value": "15.1"
              },
              {
                "dims": {
                          "REGION": "Western Pacific",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Male",
                          "YEAR": "2005"
                        },
                "Value": "14.8"
              },
              {
                "dims": {
                          "REGION": "Western Pacific",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Male",
                          "YEAR": "2000"
                        },
                "Value": "14.2"
              },
              {
                "dims": {
                          "REGION": "Western Pacific",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Female",
                          "YEAR": "2016"
                        },
                "Value": "17.6"
              },
              {
                "dims": {
                          "REGION": "Western Pacific",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Female",
                          "YEAR": "2015"
                        },
                "Value": "17.5"
              },
              {
                "dims": {
                          "REGION": "Western Pacific",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Female",
                          "YEAR": "2010"
                        },
                "Value": "17.3"
              },
              {
                "dims": {
                          "REGION": "Western Pacific",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Female",
                          "YEAR": "2005"
                        },
                "Value": "16.9"
              },
              {
                "dims": {
                          "REGION": "Western Pacific",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Female",
                          "YEAR": "2000"
                        },
                "Value": "16.3"
              },
              {
                "dims": {
                          "REGION": "(WHO) Global",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2016"
                        },
                "Value": "63.3"
              },
              {
                "dims": {
                          "REGION": "(WHO) Global",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2015"
                        },
                "Value": "63.0"
              },
              {
                "dims": {
                          "REGION": "(WHO) Global",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2010"
                        },
                "Value": "61.7"
              },
              {
                "dims": {
                          "REGION": "(WHO) Global",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2005"
                        },
                "Value": "60.0"
              },
              {
                "dims": {
                          "REGION": "(WHO) Global",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2000"
                        },
                "Value": "58.5"
              },
              {
                "dims": {
                          "REGION": "(WHO) Global",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Male",
                          "YEAR": "2016"
                        },
                "Value": "62.0"
              },
              {
                "dims": {
                          "REGION": "(WHO) Global",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Male",
                          "YEAR": "2015"
                        },
                "Value": "61.7"
              },
              {
                "dims": {
                          "REGION": "(WHO) Global",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Male",
                          "YEAR": "2010"
                        },
                "Value": "60.4"
              },
              {
                "dims": {
                          "REGION": "(WHO) Global",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Male",
                          "YEAR": "2005"
                        },
                "Value": "58.7"
              },
              {
                "dims": {
                          "REGION": "(WHO) Global",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Male",
                          "YEAR": "2000"
                        },
                "Value": "57.2"
              },
              {
                "dims": {
                          "REGION": "(WHO) Global",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Female",
                          "YEAR": "2016"
                        },
                "Value": "64.8"
              },
              {
                "dims": {
                          "REGION": "(WHO) Global",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Female",
                          "YEAR": "2015"
                        },
                "Value": "64.5"
              },
              {
                "dims": {
                          "REGION": "(WHO) Global",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Female",
                          "YEAR": "2010"
                        },
                "Value": "63.1"
              },
              {
                "dims": {
                          "REGION": "(WHO) Global",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Female",
                          "YEAR": "2005"
                        },
                "Value": "61.3"
              },
              {
                "dims": {
                          "REGION": "(WHO) Global",
                          "GHO": "Healthy life expectancy (HALE) at birth (years)",
                          "SEX": "Female",
                          "YEAR": "2000"
                        },
                "Value": "59.9"
              },
              {
                "dims": {
                          "REGION": "(WHO) Global",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2016"
                        },
                "Value": "15.8"
              },
              {
                "dims": {
                          "REGION": "(WHO) Global",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2015"
                        },
                "Value": "15.7"
              },
              {
                "dims": {
                          "REGION": "(WHO) Global",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2010"
                        },
                "Value": "15.3"
              },
              {
                "dims": {
                          "REGION": "(WHO) Global",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2005"
                        },
                "Value": "14.8"
              },
              {
                "dims": {
                          "REGION": "(WHO) Global",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Both sexes",
                          "YEAR": "2000"
                        },
                "Value": "14.3"
              },
              {
                "dims": {
                          "REGION": "(WHO) Global",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Male",
                          "YEAR": "2016"
                        },
                "Value": "14.8"
              },
              {
                "dims": {
                          "REGION": "(WHO) Global",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Male",
                          "YEAR": "2015"
                        },
                "Value": "14.7"
              },
              {
                "dims": {
                          "REGION": "(WHO) Global",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Male",
                          "YEAR": "2010"
                        },
                "Value": "14.3"
              },
              {
                "dims": {
                          "REGION": "(WHO) Global",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Male",
                          "YEAR": "2005"
                        },
                "Value": "13.7"
              },
              {
                "dims": {
                          "REGION": "(WHO) Global",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Male",
                          "YEAR": "2000"
                        },
                "Value": "13.2"
              },
              {
                "dims": {
                          "REGION": "(WHO) Global",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Female",
                          "YEAR": "2016"
                        },
                "Value": "16.8"
              },
              {
                "dims": {
                          "REGION": "(WHO) Global",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Female",
                          "YEAR": "2015"
                        },
                "Value": "16.6"
              },
              {
                "dims": {
                          "REGION": "(WHO) Global",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Female",
                          "YEAR": "2010"
                        },
                "Value": "16.2"
              },
              {
                "dims": {
                          "REGION": "(WHO) Global",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Female",
                          "YEAR": "2005"
                        },
                "Value": "15.8"
              },
              {
                "dims": {
                          "REGION": "(WHO) Global",
                          "GHO": "Healthy life expectancy (HALE) at age 60 (years)",
                          "SEX": "Female",
                          "YEAR": "2000"
                        },
                "Value": "15.4"
              }
      ]
};